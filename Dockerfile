FROM registry.gitlab.com/modioab/base-image/fedora-31/python:master

ARG URL=unknown
ARG COMMIT=unknown
ARG BRANCH=unknown
ARG HOST=unknown
ARG DATE=unknown

LABEL "se.modio.ci.url"=$URL "se.modio.ci.branch"=$BRANCH "se.modio.ci.commit"=$COMMIT "se.modio.ci.host"=$HOST "se.modio.ci.date"=$DATE

RUN mkdir /data && \
  echo foo:x:102400:100:foo:/data:/sbin/nologin >> /etc/passwd

USER 102400
WORKDIR /data
ADD index.html /data/

EXPOSE 8000
CMD ["python3", "-m", "http.server"]
